package de.brychzy.supportbot;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;

/**
 * Erstellt von Nils am 06.06.2016 um 12:58.
 */
public class SupportBot {

    protected static TS3Config ts3Config;
    protected static TS3Query ts3Query;
    protected static TS3Api ts3Api;

    private static String queryHost = "163.172.194.126";
    private static Integer queryPort = 9987;
    private static Level queryDebugLevel = Level.OFF;
    private static String queryLogin = "serveradmin";
    private static String queryPassword = "sPJMObec";
    private static String queryNickname = "Dezzy.DE » SupportBot";

    private static ArrayList<Integer> supportGroupIds = new ArrayList<>(Arrays.asList(1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045));

    private static Integer supportWaitChannel = 3493;
    //private static ArrayList<Integer> supportProgressChannel = new ArrayList<>(Arrays.asList(7, 8, 9));
    //private static Integer supportReadyChannel = 3493;

    private static ArrayList<String> supportUserMessages = new ArrayList<>(Arrays.asList("Es sind %i Supporter Online", "Bitte habe Geduld"));
    private static ArrayList<String> supportTeamMessages = new ArrayList<>(Arrays.asList("-------------------------", "%s wartet im Support!", "-------------------------"));

    public static void main(String[] args) {


        setTs3Config(new TS3Config());
        getTs3Config().setHost(getQueryHost());
        getTs3Config().setDebugLevel(getQueryDebugLevel());
        getTs3Config().setConnectionHandler(new SupportBotConnectionHandler());

        setTs3Query(new TS3Query(getTs3Config()));
        getTs3Query().connect();
    }

    public static void init() {
        setTs3Api(getTs3Query().getApi());
        getTs3Api().login(getQueryLogin(), getQueryPassword());
        getTs3Api().selectVirtualServerByPort(getQueryPort());
        getTs3Api().setNickname(getQueryNickname());

        getTs3Api().registerEvent(TS3EventType.CHANNEL);
        getTs3Api().addTS3Listeners(new SupportBotListener());
    }

    public static TS3Config getTs3Config() {
        return ts3Config;
    }

    public static void setTs3Config(TS3Config ts3Config) {
        SupportBot.ts3Config = ts3Config;
    }

    public static TS3Query getTs3Query() {
        return ts3Query;
    }

    public static void setTs3Query(TS3Query ts3Query) {
        SupportBot.ts3Query = ts3Query;
    }

    public static TS3Api getTs3Api() {
        return ts3Api;
    }

    public static void setTs3Api(TS3Api ts3Api) {
        SupportBot.ts3Api = ts3Api;
    }


    public static String getQueryHost() {
        return queryHost;
    }

    public static void setQueryHost(String queryHost) {
        SupportBot.queryHost = queryHost;
    }

    public static Integer getQueryPort() {
        return queryPort;
    }

    public static void setQueryPort(Integer queryPort) {
        SupportBot.queryPort = queryPort;
    }

    public static Level getQueryDebugLevel() {
        return queryDebugLevel;
    }

    public static void setQueryDebugLevel(Level queryDebugLevel) {
        SupportBot.queryDebugLevel = queryDebugLevel;
    }

    public static String getQueryLogin() {
        return queryLogin;
    }

    public static void setQueryLogin(String queryLogin) {
        SupportBot.queryLogin = queryLogin;
    }

    public static String getQueryPassword() {
        return queryPassword;
    }

    public static void setQueryPassword(String queryPassword) {
        SupportBot.queryPassword = queryPassword;
    }

    public static String getQueryNickname() {
        return queryNickname;
    }

    public static void setQueryNickname(String queryNickname) {
        SupportBot.queryNickname = queryNickname;
    }

    public static ArrayList<Integer> getSupportGroupIds() {
        return supportGroupIds;
    }

    public static void setSupportGroupIds(ArrayList<Integer> supportGroupIds) {
        SupportBot.supportGroupIds = supportGroupIds;
    }

    public static Integer getSupportWaitChannel() {
        return supportWaitChannel;
    }

    public static void setSupportWaitChannel(Integer supportWaitChannel) {
        SupportBot.supportWaitChannel = supportWaitChannel;
    }


    public static ArrayList<String> getSupportUserMessages() {
        return supportUserMessages;
    }

    public static void setSupportUserMessages(ArrayList<String> supportUserMessages) {
        SupportBot.supportUserMessages = supportUserMessages;
    }

    public static ArrayList<String> getSupportTeamMessages() {
        return supportTeamMessages;
    }

    public static void setSupportTeamMessages(ArrayList<String> supportTeamMessages) {
        SupportBot.supportTeamMessages = supportTeamMessages;
    }
}
