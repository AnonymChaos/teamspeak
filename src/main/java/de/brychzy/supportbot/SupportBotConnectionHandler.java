package de.brychzy.supportbot;

import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.reconnect.ConnectionHandler;

/**
 * Erstellt von Nils am 06.06.2016 um 14:02.
 */
public class SupportBotConnectionHandler implements ConnectionHandler {

    public void onConnect(TS3Query ts3Query) {
        SupportBot.init();
    }

    public void onDisconnect(TS3Query ts3Query) {

    }
}
