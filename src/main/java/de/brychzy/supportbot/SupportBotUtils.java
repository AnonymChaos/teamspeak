package de.brychzy.supportbot;

/**
 * Erstellt von Nils am 06.06.2016 um 14:23.
 */
public class SupportBotUtils {
    private static Integer onlineSupporter = 0;

    public static void notifySupporter(Integer clientId) {
        setOnlineSupporter(0);
        SupportBot.getTs3Api().getClients().forEach(client -> {
            Boolean isSupporter = SupportBot.getSupportGroupIds().stream().anyMatch(groupId -> {
                for (Integer group : client.getServerGroups()) {
                    if (group.equals(groupId)) {
                        return true;
                    }
                }
                return false;
            });

            if (isSupporter) {
                SupportBot.getSupportTeamMessages().forEach(message -> {
                    SupportBot.getTs3Api().sendPrivateMessage(client.getId(), message.replace("%s", SupportBot.getTs3Api().getClientInfo(clientId).getNickname()));
                });
                setOnlineSupporter(getOnlineSupporter() + 1);
            }
        });
    }

    public static void notifyUser(Integer clientId) {
        SupportBot.getSupportUserMessages().forEach(message -> SupportBot.getTs3Api().sendPrivateMessage(clientId, message.replace("%i", String.valueOf(getOnlineSupporter()))));
    }

    public static Integer getOnlineSupporter() {
        return onlineSupporter;
    }

    public static void setOnlineSupporter(Integer onlineSupporter) {
        SupportBotUtils.onlineSupporter = onlineSupporter;
    }
}
