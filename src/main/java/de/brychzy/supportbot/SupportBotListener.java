package de.brychzy.supportbot;

import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;

/**
 * Erstellt von Nils am 06.06.2016 um 13:57.
 */
public class SupportBotListener extends TS3EventAdapter {

    @Override
    public void onClientMoved(ClientMovedEvent e) {
        if (e.getTargetChannelId() != SupportBot.getSupportWaitChannel()) {
            return;
        }
        SupportBotUtils.notifySupporter(e.getClientId());
        SupportBotUtils.notifyUser(e.getClientId());
    }
}
